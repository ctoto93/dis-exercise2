package de.dis2011.data;

import java.sql.*;

public class Apartment extends Estate {
    private int floor;
    private double rent;
    private int rooms;
    private boolean balcony;
    private boolean builtInKitchen;
    private EstateAgent estateAgent;

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public boolean hasBalcony() {
        return balcony;
    }

    public void setBalcony(boolean balcony) {
        this.balcony = balcony;
    }

    public boolean hasBuiltInKitchen() {
        return builtInKitchen;
    }

    public void setBuiltInKitchen(boolean builtInKitchen) {
        this.builtInKitchen = builtInKitchen;
    }

    public EstateAgent getEstateAgent() {
        return estateAgent;
    }

    public void setEstateAgent(EstateAgent estateAgent) {
        this.estateAgent = estateAgent;
    }

    public void save() {
        // Hole Verbindung
        Connection con = DB2ConnectionManager.getInstance().getConnection();

        try {
            if (getId() == -1) {
                String insertSQL = "INSERT INTO APARTMENTS(city, postal_code, street, street_number, square_area, " +
                                    "floor, rent, rooms, balcony, built_in_kitchen, estate_agent_id) " +
                                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                PreparedStatement pstmt = con.prepareStatement(insertSQL,
                        Statement.RETURN_GENERATED_KEYS);

                // Setze Anfrageparameter und fC<hre Anfrage aus
                pstmt.setString(1, getCity());
                pstmt.setString(2, getPostalCode());
                pstmt.setString(3, getStreet());
                pstmt.setString(4, getStreetNumber());
                pstmt.setDouble(5, getSquareArea());
                pstmt.setInt(6, getFloor());
                pstmt.setDouble(7, getRent());
                pstmt.setInt(8, getRooms());
                pstmt.setBoolean(9, hasBalcony());
                pstmt.setBoolean(10, hasBuiltInKitchen());
                pstmt.setInt(11, getEstateAgent().getId());
                pstmt.executeUpdate();

                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    setId(rs.getInt(1));
                }

                rs.close();
                pstmt.close();
            } else {
                String updateSQL = "UPDATE APARTMENTS SET city = ?, postal_code = ?, street = ?, street_number = ?," +
                        "square_area = ?, floor = ?, rent = ?, rooms = ?, balcony = ?, " +
                        "built_in_kitchen = ?, estate_agent_id = ? WHERE id = ?";
                PreparedStatement pstmt = con.prepareStatement(updateSQL);

                pstmt.setString(1, getCity());
                pstmt.setString(2, getPostalCode());
                pstmt.setString(3, getStreet());
                pstmt.setString(4, getStreetNumber());
                pstmt.setDouble(5, getSquareArea());
                pstmt.setInt(6, getFloor());
                pstmt.setDouble(7, getRent());
                pstmt.setInt(8, getRooms());
                pstmt.setBoolean(9, hasBalcony());
                pstmt.setBoolean(10, hasBuiltInKitchen());
                pstmt.setInt(11, getEstateAgent().getId());
                pstmt.setInt(12, getId());
                pstmt.executeUpdate();

                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Apartment load(int id) {
        try {
            // Hole Verbindung
            Connection con = DB2ConnectionManager.getInstance().getConnection();

            // Erzeuge Anfrage
            String selectSQL = "SELECT * FROM APARTMENTS WHERE id = ?";
            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt.setInt(1, id);

            // Führe Anfrage aus
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                Apartment a = new Apartment();
                a.setId(rs.getInt("id"));
                a.setCity(rs.getString("city"));
                a.setPostalCode(rs.getString("postal_code"));
                a.setStreet(rs.getString("street"));
                a.setStreetNumber(rs.getString("street_number"));
                a.setSquareArea(rs.getDouble("square_area"));
                a.setFloor(rs.getInt("floor"));
                a.setRooms(rs.getInt("rooms"));
                a.setBalcony(rs.getBoolean("balcony"));
                a.setBuiltInKitchen(rs.getBoolean("built_in_kitchen"));

                EstateAgent ea = EstateAgent.load(rs.getInt("estate_agent_id"));
                a.setEstateAgent(ea);

                rs.close();
                pstmt.close();
                return a;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void delete() {
        Connection con = DB2ConnectionManager.getInstance().getConnection();

        try {
            // FC<ge neues Element hinzu, wenn das Objekt noch keine ID hat.
            String deleteSql = "DELETE FROM APARTMENTS WHERE id = ?";

            PreparedStatement pstmt = con.prepareStatement(deleteSql);
            pstmt.setInt(1, getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
