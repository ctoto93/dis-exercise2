package de.dis2011.data;

import java.sql.*;

public class House extends Estate {
    private int floor;
    private double price;
    private boolean garden;
    private EstateAgent estateAgent;

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean hasGarden() {
        return garden;
    }

    public void setGarden(boolean garden) {
        this.garden = garden;
    }

    public EstateAgent getEstateAgent() {
        return estateAgent;
    }

    public void setEstateAgent(EstateAgent estateAgent) {
        this.estateAgent = estateAgent;
    }

    public void save() {
        // Hole Verbindung
        Connection con = DB2ConnectionManager.getInstance().getConnection();

        try {
            if (getId() == -1) {
                String insertSQL = "INSERT INTO HOUSES(city, postal_code, street, street_number, square_area, " +
                                    "floors, price, garden, estate_agent_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

                PreparedStatement pstmt = con.prepareStatement(insertSQL,
                        Statement.RETURN_GENERATED_KEYS);

                // Setze Anfrageparameter und fC<hre Anfrage aus
                pstmt.setString(1, getCity());
                pstmt.setString(2, getPostalCode());
                pstmt.setString(3, getStreet());
                pstmt.setString(4, getStreetNumber());
                pstmt.setDouble(5, getSquareArea());
                pstmt.setInt(6, getFloor());
                pstmt.setDouble(7, getPrice());
                pstmt.setBoolean(8, hasGarden());
                pstmt.setInt(9, getEstateAgent().getId());
                pstmt.executeUpdate();

                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    setId(rs.getInt(1));
                }

                rs.close();
                pstmt.close();
            } else {
                String updateSQL = "UPDATE HOUSES SET city = ?, postal_code = ?, street = ?, street_number = ?," +
                        "square_area = ?, floors = ?, price = ?, garden = ?, estate_agent_id = ? WHERE id = ?";
                PreparedStatement pstmt = con.prepareStatement(updateSQL);

                pstmt.setString(1, getCity());
                pstmt.setString(2, getPostalCode());
                pstmt.setString(3, getStreet());
                pstmt.setString(4, getStreetNumber());
                pstmt.setDouble(5, getSquareArea());
                pstmt.setInt(6, getFloor());
                pstmt.setDouble(7, getPrice());
                pstmt.setBoolean(8, hasGarden());
                pstmt.setInt(9, getEstateAgent().getId());
                pstmt.setInt(10, getId());
                pstmt.executeUpdate();

                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static House load(int id) {
        try {
            // Hole Verbindung
            Connection con = DB2ConnectionManager.getInstance().getConnection();

            // Erzeuge Anfrage
            String selectSQL = "SELECT * FROM HOUSES WHERE id = ?";
            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt.setInt(1, id);

            // Führe Anfrage aus
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                House a = new House();
                a.setId(rs.getInt("id"));
                a.setCity(rs.getString("city"));
                a.setPostalCode(rs.getString("postal_code"));
                a.setStreet(rs.getString("street"));
                a.setStreetNumber(rs.getString("street_number"));
                a.setSquareArea(rs.getDouble("square_area"));
                a.setFloor(rs.getInt("floors"));
                a.setPrice(rs.getDouble("price"));
                a.setGarden(rs.getBoolean("garden"));

                EstateAgent ea = EstateAgent.load(rs.getInt("estate_agent_id"));
                a.setEstateAgent(ea);

                rs.close();
                pstmt.close();
                return a;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void delete() {
        Connection con = DB2ConnectionManager.getInstance().getConnection();

        try {
            // FC<ge neues Element hinzu, wenn das Objekt noch keine ID hat.
            String deleteSql = "DELETE FROM HOUSES WHERE id = ?";

            PreparedStatement pstmt = con.prepareStatement(deleteSql);
            pstmt.setInt(1, getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
