package de.dis2011.data;

import java.sql.*;
import java.util.Date;

public class TenancyContract extends Contract {
    private Date startDate;
    private int duration;
    private double additionalCosts;
    private Apartment apartment;
    private Person person;


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getAdditionalCosts() {
        return additionalCosts;
    }

    public void setAdditionalCosts(double additionalCosts) {
        this.additionalCosts = additionalCosts;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public static TenancyContract load(int contract) {
        try {
            // Hole Verbindung
            Connection con = DB2ConnectionManager.getInstance().getConnection();

            // Erzeuge Anfrage
            String selectSQL = "SELECT * FROM TENANCY_CONTRACT WHERE contract_no = ?";
            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt.setInt(1, contract);

            // Führe Anfrage aus
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                TenancyContract tc = new TenancyContract();
                tc.setId(rs.getInt("contract_no"));
                tc.setPlace(rs.getString("place"));
                tc.setDate(rs.getDate("date"));
                tc.setStartDate(rs.getDate("start_date"));
                tc.setDuration(rs.getInt("duration"));
                tc.setAdditionalCosts(rs.getDouble("additional_costs"));

                tc.setApartment(Apartment.load(rs.getInt("apartment_id")));
                tc.setPerson(Person.load(rs.getInt("person_id")));

                rs.close();
                pstmt.close();
                return tc;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void save() {
        Connection con = DB2ConnectionManager.getInstance().getConnection();

        try {
            if (getId() == -1) {
                String insertSQL = "INSERT INTO TENANCY_CONTRACT(place, date, start_date, duration, additional_costs, person_id, apartment_id) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)";

                PreparedStatement pstmt = con.prepareStatement(insertSQL,
                        Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, getPlace());
                pstmt.setDate(2, new java.sql.Date(getDate().getTime()));
                pstmt.setDate(3, new java.sql.Date(getStartDate().getTime()));
                pstmt.setInt(4, getDuration());
                pstmt.setDouble(5, getAdditionalCosts());
                pstmt.setInt(6, getPerson().getId());
                pstmt.setInt(7, getApartment().getId());
                pstmt.executeUpdate();

                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    setId(rs.getInt(1));
                }

                rs.close();
                pstmt.close();
            } else {
                String updateSQL = "UPDATE TENANCY_CONTRACT SET date = ?, place = ?, start_date = ?, duration = ?, additional_costs = ?, person_id = ?, apartment_id WHERE id = ?";
                PreparedStatement pstmt = con.prepareStatement(updateSQL);

                pstmt.setDate(1, new java.sql.Date(getDate().getTime()));
                pstmt.setString(2, getPlace());
                pstmt.setDate(3, new java.sql.Date(getStartDate().getTime()));
                pstmt.setInt(4, getDuration());
                pstmt.setDouble(5, getAdditionalCosts());
                pstmt.setInt(6, getPerson().getId());
                pstmt.setInt(7, getApartment().getId());
                pstmt.setInt(8, getId());
                pstmt.executeUpdate();

                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
