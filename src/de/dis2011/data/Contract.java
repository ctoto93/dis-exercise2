package de.dis2011.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

public class Contract {
    private int id = -1;
    private Date date;
    private String place;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void print() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println("Contract details: \n" + gson.toJson(this));
    }


}
