package de.dis2011.data;

import java.sql.*;

public class PurchaseContract extends Contract {
    private int installments;
    private double intrest;
    private House house;
    private Person person;


    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public double getIntrest() {
        return intrest;
    }

    public void setIntrest(double intrest) {
        this.intrest = intrest;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public static PurchaseContract load(int contract) {
        try {
            // Hole Verbindung
            Connection con = DB2ConnectionManager.getInstance().getConnection();

            // Erzeuge Anfrage
            String selectSQL = "SELECT * FROM PURCHASE_CONTRACT WHERE contract_no = ?";
            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt.setInt(1, contract);

            // Führe Anfrage aus
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                PurchaseContract tc = new PurchaseContract();
                tc.setId(rs.getInt("contract_no"));
                tc.setPlace(rs.getString("place"));
                tc.setDate(rs.getDate("date"));
                tc.setInstallments(rs.getInt("installments"));
                tc.setIntrest(rs.getDouble("intrest"));

                tc.setHouse(House.load(rs.getInt("house_id")));
                tc.setPerson(Person.load(rs.getInt("person_id")));

                rs.close();
                pstmt.close();
                return tc;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void save() {
        Connection con = DB2ConnectionManager.getInstance().getConnection();

        try {
            if (getId() == -1) {
                String insertSQL = "INSERT INTO PURCHASE_CONTRACT(place, date, installments, intrest, person_id, house_id) " +
                        "VALUES (?, ?, ?, ?, ?, ?)";

                PreparedStatement pstmt = con.prepareStatement(insertSQL,
                        Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, getPlace());
                pstmt.setDate(2, new java.sql.Date(getDate().getTime()));
                pstmt.setInt(3, getInstallments());
                pstmt.setDouble(4, getIntrest());
                pstmt.setInt(5, getPerson().getId());
                pstmt.setInt(6, getHouse().getId());
                pstmt.executeUpdate();

                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    setId(rs.getInt(1));
                }

                rs.close();
                pstmt.close();
            } else {
                String updateSQL = "UPDATE PURCHASE_CONTRACT SET date = ?, place = ?, installments = ?, intrest = ?, person_id = ?, apartment_id WHERE id = ?";
                PreparedStatement pstmt = con.prepareStatement(updateSQL);

                pstmt.setDate(1, new java.sql.Date(getDate().getTime()));
                pstmt.setString(2, getPlace());
                pstmt.setInt(3, getInstallments());
                pstmt.setDouble(4, getIntrest());
                pstmt.setInt(5, getPerson().getId());
                pstmt.setInt(6, getHouse().getId());
                pstmt.setInt(7, getId());
                pstmt.executeUpdate();

                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
