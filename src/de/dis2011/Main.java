package de.dis2011;

import com.google.gson.Gson;
import de.dis2011.data.Apartment;
import de.dis2011.data.EstateAgent;
import de.dis2011.data.House;
import de.dis2011.data.Person;
import de.dis2011.data.TenancyContract;
import de.dis2011.data.PurchaseContract;

import java.util.ArrayList;

/**
 * Hauptklasse
 */
public class Main {
	/**
	 * Startet die Anwendung
	 */
	private static final String ADMIN_USERNAME = "admin";
	private static final String ADMIN_PASSWORD = "admin";

	public static void main(String[] args) {
		showMainMenu();
	}
	
	/**
	 * Zeigt das Hauptmenü
	 */
	public static void showMainMenu() {
		//Menüoptionen
		final int LOGIN = 0;
		final int ADMIN = 1;
		final int QUIT = 99;
		
		//Erzeuge Menü
		Menu mainMenu = new Menu("WELCOME TO ESTATE SYSTEM");
		mainMenu.addEntry("Agent Login", LOGIN);
		mainMenu.addEntry("Admin Menu", ADMIN);
		mainMenu.addEntry("Exit", QUIT);
		
		//Verarbeite Eingabe
		while(true) {
			int response = mainMenu.show();
			
			switch(response) {
				case LOGIN:
					String login = FormUtil.readString("Login");
					String password = FormUtil.readString("Password");

					EstateAgent estateAgent = EstateAgent.getAgentByCredential(login, password);
					if (estateAgent == null) {
						System.out.println("Incorrect credential");
						break;
					}
					showAgentMenu(estateAgent);
					break;
				case ADMIN:
					String adminUsername = FormUtil.readString("Admin username");
					String adminPassword = FormUtil.readString("Admin password");
					if (ADMIN_USERNAME.equals(adminUsername) && ADMIN_PASSWORD.equals(adminPassword)) {
						showAdminMenu();
					}
					else {
						System.out.println("Incorrect credential");
					}
					break;
				case QUIT:
					return;
			}
		}
	}
	
	/**
	 * Zeigt die Maklerverwaltung
	 */
	public static void showAdminMenu() {

		final int NEW_ESTATE_AGENT = 0;
		final int SHOW_AGENT = 1;
		final int EDIT_AGENT = 2;
		final int DELETE_AGENT = 3;
		final int BACK = 99;

		Menu adminMenu = new Menu("Estate Agent Management");
		adminMenu.addEntry("New Agent", NEW_ESTATE_AGENT);
		adminMenu.addEntry("Show All Agents", SHOW_AGENT);
		adminMenu.addEntry("Edit Agent by ID", EDIT_AGENT);
		adminMenu.addEntry("Delete an Agent by ID", DELETE_AGENT);
		adminMenu.addEntry("Back", BACK);

		while(true) {
			int response = adminMenu.show();
			
			switch(response) {
				case NEW_ESTATE_AGENT:
					newEstateAgent();
					break;
				case SHOW_AGENT:
					System.out.println("Loading please wait");
					ArrayList<EstateAgent> estateAgents = EstateAgent.listAllEstateAgents();
					for (EstateAgent estateAgent : estateAgents) {
						System.out.println("Id: " + estateAgent.getId());
						System.out.println("Name: " + estateAgent.getName());
						System.out.println("Address: " + estateAgent.getAddress());
						System.out.println("DONE");
					}
					break;
				case EDIT_AGENT:
					int id = FormUtil.readInt("Enter agent's id to be edited");
					EstateAgent estateAgent = EstateAgent.load(id);
					estateAgent.setName(FormUtil.readString("Name"));
					estateAgent.setAddress(FormUtil.readString("Adresse"));
					estateAgent.setLogin(FormUtil.readString("Login"));
					estateAgent.setPassword(FormUtil.readString("Passwort"));
					estateAgent.save();
					break;
				case DELETE_AGENT:
					id = FormUtil.readInt("Enter agent's id to be deleted");
					estateAgent = EstateAgent.load(id);
					estateAgent.delete(id);
					break;
				case BACK:
					return;
			}
		}
	}
	
	/**
	 * Legt einen neuen EstateAgent an, nachdem der Benutzer
	 * die entprechenden Daten eingegeben hat.
	 */
	public static void newEstateAgent() {
		EstateAgent m = new EstateAgent();
		
		m.setName(FormUtil.readString("Name"));
		m.setAddress(FormUtil.readString("Adresse"));
		m.setLogin(FormUtil.readString("Login"));
		m.setPassword(FormUtil.readString("Passwort"));
		m.save();
		
		System.out.println("EstateAgent mit der ID "+m.getId()+" wurde erzeugt.");
	}

	public static void showAgentMenu(EstateAgent loggedInAgent) {
		final int INSERT_NEW_PERSON = 0;
		final int CREATE_APARTMENT = 1;
		final int UPDATE_APARTMENT = 2;
		final int DELETE_APARTMENT = 3;
		final int CREATE_HOUSE = 4;
		final int UPDATE_HOUSE= 5;
		final int DELETE_HOUSE = 6;
		final int CREATE_PURCHASE_CONTRACT = 7;
		final int SHOW_PURCHASE_CONTRACT = 8;
		final int CREATE_TENANCY_CONTRACT = 9;
		final int SHOW_TENANCY_CONTRACT = 10;
		final int BACK = 99;

		Menu agentMenu = new Menu("Agent Menu");
		agentMenu.addEntry("Insert New Person", INSERT_NEW_PERSON);
		agentMenu.addEntry("Create Apartment", CREATE_APARTMENT);
		agentMenu.addEntry("Update Apartment", UPDATE_APARTMENT);
		agentMenu.addEntry("Delete Apartment", DELETE_APARTMENT);
		agentMenu.addEntry("Create House", CREATE_HOUSE);
		agentMenu.addEntry("Update House", UPDATE_HOUSE);
		agentMenu.addEntry("Delete House", DELETE_HOUSE);
		agentMenu.addEntry("Create Purchase Contract", CREATE_PURCHASE_CONTRACT);
		agentMenu.addEntry("Show Purchase Contract", SHOW_PURCHASE_CONTRACT);
		agentMenu.addEntry("Create Tenancy Contract", CREATE_TENANCY_CONTRACT);
		agentMenu.addEntry("Show Tenancy Contract", SHOW_TENANCY_CONTRACT);
		agentMenu.addEntry("Back", BACK);


		while(true) {
			int response = agentMenu.show();
			switch (response) {
				case INSERT_NEW_PERSON:
					Person p = new Person();
					p.setFirstname(FormUtil.readString("Firstname"));
					p.setName(FormUtil.readString("Name (Surname)"));
					p.setAddress(FormUtil.readString("Address"));
					p.save();

					System.out.println("Created person with ID "+p.getId());
					break;
				case CREATE_APARTMENT:
					Apartment a = new Apartment();
					a.setCity(FormUtil.readString("City"));
					a.setPostalCode(FormUtil.readString("Postal Code"));
					a.setStreet(FormUtil.readString("Street"));
					a.setStreetNumber(FormUtil.readString("Street Number"));
					a.setSquareArea(FormUtil.readDouble("Square Area"));
					a.setFloor(FormUtil.readInt("Floor"));
					a.setRooms(FormUtil.readInt("Rooms"));
					a.setBalcony(FormUtil.readBoolean("Has a balcony? Yes(1) / No(0)"));
					a.setBuiltInKitchen(FormUtil.readBoolean("Has built in kitchen? Yes(1) / No(0)"));
					a.setEstateAgent(loggedInAgent);

					a.save();
					System.out.println("Created an apartment with ID "+a.getId() + "for agent id: " + loggedInAgent.getId());
					break;
				case UPDATE_APARTMENT:
					int id = FormUtil.readInt("Apartment Id to be edited");
					a = Apartment.load(id);

					if (a.getEstateAgent().getId() != loggedInAgent.getId()) {
						System.out.println("Your are not permitted to edit this estate!!!");
						continue;
					}
					a.setCity(FormUtil.readString("City"));
					a.setPostalCode(FormUtil.readString("Postal Code"));
					a.setStreet(FormUtil.readString("Street"));
					a.setStreetNumber(FormUtil.readString("Street Number"));
					a.setSquareArea(FormUtil.readDouble("Square Area"));
					a.setFloor(FormUtil.readInt("Floor"));
					a.setRooms(FormUtil.readInt("Rooms"));
					a.setBalcony(FormUtil.readBoolean("Has a balcony? Yes(1) / No(0)"));
					a.setBuiltInKitchen(FormUtil.readBoolean("Has built in kitchen? Yes(1) / No(0)"));
					a.setEstateAgent(loggedInAgent);

					a.save();
					break;
				case DELETE_APARTMENT:
					id = FormUtil.readInt("Apartment Id to be deleted");
					a = Apartment.load(id);
					if (a.getEstateAgent().getId() != loggedInAgent.getId()) {
						System.out.println("Your are not permitted to delete this estate!!!");
						continue;
					}
					a.delete();
					break;
				case CREATE_HOUSE:
					House h = new House();
					h.setCity(FormUtil.readString("City"));
					h.setPostalCode(FormUtil.readString("Postal Code"));
					h.setStreet(FormUtil.readString("Street"));
					h.setStreetNumber(FormUtil.readString("Street Number"));
					h.setSquareArea(FormUtil.readDouble("Square Area"));
					h.setFloor(FormUtil.readInt("Floors"));
					h.setPrice(FormUtil.readDouble("Price"));
					h.setGarden(FormUtil.readBoolean("Has a garden? Yes(1) / No(0)"));
					h.setEstateAgent(loggedInAgent);

					h.save();
					System.out.println("Created an house with ID "+h.getId() + " for agent id: " + loggedInAgent.getId());
					break;
				case UPDATE_HOUSE:
					int house_id = FormUtil.readInt("House Id to be edited");
					h = House.load(house_id);

					if (h.getEstateAgent().getId() != loggedInAgent.getId()) {
						System.out.println("Your are not permitted to edit this estate!!!");
						continue;
					}
					h.setCity(FormUtil.readString("City"));
					h.setPostalCode(FormUtil.readString("Postal Code"));
					h.setStreet(FormUtil.readString("Street"));
					h.setStreetNumber(FormUtil.readString("Street Number"));
					h.setSquareArea(FormUtil.readDouble("Square Area"));
					h.setFloor(FormUtil.readInt("Floors"));
					h.setPrice(FormUtil.readInt("Price"));
					h.setGarden(FormUtil.readBoolean("Has a garden? Yes(1) / No(0)"));
					h.setEstateAgent(loggedInAgent);

					h.save();
					break;
				case DELETE_HOUSE:
					house_id = FormUtil.readInt("House Id to be deleted");
					h = House.load(house_id);
					if (h.getEstateAgent().getId() != loggedInAgent.getId()) {
						System.out.println("Your are not permitted to delete this estate!!!");
						continue;
					}
					h.delete();
					break;
				case CREATE_PURCHASE_CONTRACT:
					h = House.load(FormUtil.readInt("House Id:"));
					p = Person.load(FormUtil.readInt("Person Id to sell the apartment: "));

					PurchaseContract pc = new PurchaseContract();
					pc.setPlace(FormUtil.readString("Place: "));
					pc.setDate(FormUtil.readDate("Contract date: "));
					pc.setInstallments(FormUtil.readInt("Installments: "));
					pc.setIntrest(FormUtil.readDouble("Interest Rate: "));
					pc.setHouse(h);
					pc.setPerson(p);

					pc.save();

					System.out.println("Created a purchase contract with ID "+pc.getId());

					break;
				case SHOW_PURCHASE_CONTRACT:
					id = FormUtil.readInt("Purchase Contract no to be shown");
					pc = PurchaseContract.load(id);
					pc.print();
					break;
				case CREATE_TENANCY_CONTRACT:
					a = Apartment.load(FormUtil.readInt("Apartment Id:"));
					p = Person.load(FormUtil.readInt("Person Id to rent the apartment: "));

					TenancyContract tc = new TenancyContract();
					tc.setPlace(FormUtil.readString("Place: "));
					tc.setDate(FormUtil.readDate("Contract date: "));
					tc.setStartDate(FormUtil.readDate("Tenancy start date: "));
					tc.setDuration(FormUtil.readInt("Duration (in months): "));
					tc.setAdditionalCosts(FormUtil.readDouble("Additional costs: "));
					tc.setApartment(a);
					tc.setPerson(p);

					tc.save();

					System.out.println("Created a tenancy contract with ID "+tc.getId());

					break;
				case SHOW_TENANCY_CONTRACT:
					id = FormUtil.readInt("Tenancy Contract no to be shown");
					tc = TenancyContract.load(id);
					tc.print();
					break;
			}
		}


	}
}
